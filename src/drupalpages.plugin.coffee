# Prepare
Async = require 'async'
Fs = require 'fs-extra'
path = require 'path'
_ = require 'lodash'

gulp = require 'gulp'
gulpRename = require 'gulp-rename'
gulpTemplate = require 'gulp-template'
gulpTemplateHtml = require 'gulp-template-html'

TaskGroup = require('taskgroup').TaskGroup

# Export Plugin
module.exports = (BasePlugin) ->

    # Define Plugin
    class DrupalpagesPlugin extends BasePlugin
        # Prepare
        feedr = @feedr ?= new (require('feedr').Feedr)
        internals = {}

        # Menus. Will be made available as templateData once menus are pulled
        internals.menus = {}

        # Redirects.
        internals.redirects = {}
        internals.redirectsRef = {}

        # Keep track of all page nids imported from drupal
        internals.importedPageNids = []
        internals.updatedPageNids = []
        internals.staticRouteUrls = []
        internals.forceGulpTemplateNids = []

        # Keep a reference of the page routes
        internals.drupalRoutesLookup = {}
        internals.staticRoutesLookup = {}

        # Keep a reference of the menus so we can see if they changed upon regen
        internals.menuRef = {}
        internals.menuRefPrev = {}
        internals.menuRefPrevExists = false
        internals.menuChanged = false

        # Tour options for booking widget.  Will be made available as templateData once pages are pulled
        internals.tourOptions = {}

        # Plugin name
        name: 'drupalpages'

        # Default config.  Can be overridden in plugin settings
        config:
            relativeDirPath: 'drupal'
            extension: ".html.eco"
            injectDocumentHelper: null
            forceGulpTemplateSections: []


        # Docpad Loaded
        docpadLoaded: () ->
            docpad = @docpad

            # Change log levels as required
            if process.env.SIMPLE_LOGGING?
                docpad.getLogger().setConfig({level: 6, color: false})

            # Chain
            @


        # Docpad Ready
        docpadReady: () ->
            # Prepare
            me = @
            docpad = @docpad
            config = @getConfig()

            internals.env = process.env.NODE_ENV or docpad?.getEnvironment()
            internals.preview = internals.env is 'preview'
            internals.menuFile = 'menu.json'
            internals.redirectsFile = 'redirects.json'

            # If we're in the preview mode, we need to update our api endpoint urls
            if internals.preview
                config.feeds.redirects.url += '?preview=1'

                feedKeys = Object.keys(config.feeds.menu)
                feedKeys.forEach (feed, key) ->
                    config.feeds.menu[feed].url += '?preview=1'

                    me.setConfig config

            # Fetch the previous menu data
            Fs.readJson path.join(docpad.getConfig().outPath, internals.menuFile), (err, menu) ->
                internals.menuRefPrev = menu unless err
                internals.menuRefPrevExists = !err

            # Chain
            @


        # Generate Before
        generateBefore: (opts) ->
            # Prepare
            docpad = @docpad

            docpad.log 'info', "Generating #{internals.env} site..."

            # Chain
            @


        # contextualizeBefore
        # Add the menu feeds to templateData
        contextualizeBefore: ({templateData}) ->
            # Prepare
            docpad = @docpad
            config = @getConfig()

            # Shuffle the footer menu feed object for easy grouping in the templates
            menuFooterTree = internals.menus.menuFooter.tree
            menuPrimaryTree = internals.menus.menuPrimary.tree
            menuFooterData = _.map(_.valuesIn(internals.menus.menuFooter.tree), 'link')
            menuPrimaryData = _.map(_.valuesIn(internals.menus.menuPrimary.tree), 'link')
            menuFooterGroups = {
                en: _.where(menuPrimaryData, {language: 'en'}).concat(_.where(menuFooterData, {language: 'en'}))
                fr: _.where(menuPrimaryData, {language: 'fr'}).concat(_.where(menuFooterData, {language: 'fr'}))
                es: _.where(menuPrimaryData, {language: 'es'}).concat(_.where(menuFooterData, {language: 'es'}))
            }

            menuFooter = {}
            _.forEach menuFooterGroups, (val, key) ->
                chunked= []
                groups = 4
                chunkSize = Math.floor (val.length/groups)
                remainder = val.length%groups

                groupsIterator = groups + 1
                while groupsIterator -= 1
                    size = chunkSize
                    if remainder >= 1
                        size += 1

                    remainder -= 1

                    pullIndexes = []
                    index = 0
                    while index < size
                        pullIndexes.push(index)
                        index += 1

                    chunked.push(_.pullAt val, pullIndexes)

                menuFooter[key] = chunked

            # Extend the template data
            templateData.drupalpages or= {}
            templateData.drupalpages.feeds or= {}
            templateData.drupalpages.feeds.menu = internals.menus
            templateData.drupalpages.feeds.menu.menuFooter = menuFooter
            templateData.drupalpages.feeds.menu.menuFooter.tree = menuFooterTree
            templateData.site.importedPageNids = internals.importedPageNids

            # Booking options
            templateData.tourOptions = internals.tourOptions

            # Chain
            @


        renderCollectionBefore: ({collection, renderPass}) ->
            # Prepare
            docpad = @docpad

            docpad.log 'info', 'Performing Render Pass', renderPass

            # Chain
            @


        # Write After
        writeAfter: ({collection}, next) ->
            # Prepare
            docpad = @docpad
            database = docpad.getDatabase()
            config = @getConfig()
            helpers = docpad.pluginsTemplateData
            updateNids = _.union internals.updatedPageNids, internals.forceGulpTemplateNids

            htmlFiles = database.findAll({layout: $in: config.layoutsUsed})

            # Create Routes Object
            htmlFiles.forEach (model, index) ->

                {language, isTranslated, translations, menuName, url, pnid} = model?.attributes or {}
                {section, alt, slug, nid} = model?.attributes?.page or {}

                route =
                    nid: nid or ''
                    pnid: pnid or ''
                    section: section or ''
                    alt: alt or ''
                    slug: slug or ''
                    language: language or ''
                    isTranslated: isTranslated or ''
                    translations: translations or ''
                    menuName: menuName or ''
                    docUrl: url or ''

                if route.nid in internals.importedPageNids
                    internals.drupalRoutesLookup[nid] = route
                else if _.endsWith url, '/tmpl'
                    internals.staticRouteUrls.push url
                    internals.staticRoutesLookup[url] = route

            menuGen = internals.menuChanged or !internals.menuRefPrevExists
            importedGen = updateNids.length > 0
            staticGen = internals.staticRouteUrls.length > 0

            # Create a taskgroup to run all tasks in parallel and avoid issues
            tasks = new TaskGroup "writeAfter tasks", concurrency: 0

            tasks.addTask 'generate menu json', (complete) ->
                generateMenuJson complete

            tasks.addTask 'generate redirects json', (complete) ->
                generateRedirectsJson complete

            generateRedirectsJson = (complete) ->
                # Generate our menu.json file
                filepath = path.join(docpad.getConfig().outPath, internals.redirectsFile)

                docpad.log 'info', "Creating redirects reference file..."

                Fs.outputJSON filepath, internals.redirectsRef, (err) ->
                    return complete err if err
                    docpad.log 'info', "Created redirects reference file"
                    complete()

            generateMenuJson = (complete) ->
                # Generate our menu.json file
                filepath = path.join(docpad.getConfig().outPath, internals.menuFile)

                docpad.log 'info', "Creating menu reference file..."

                Fs.outputJSON filepath, internals.menuRef, (err) ->
                    return complete err if err
                    docpad.log 'info', "Created menu reference file"
                    complete()

            # Gulp menu templates
            if menuGen or importedGen or staticGen
                # Prepare the tasks
                generateNids = if menuGen then _.uniq internals.importedPageNids else _.uniq updateNids
                generateUrls = internals.staticRouteUrls

                internals.routesProcessed = 0
                internals.routesTotal = generateNids.length + generateUrls.length

                docpad.log 'info', "Post-processing templates..."

                generateNids.forEach (nid) ->
                    tasks.addTask "process route #{nid}", (complete) ->
                        processOneRoute nid, complete

                generateUrls.forEach (url) ->
                    tasks.addTask "process route #{url}", (complete) ->
                        processOneRoute url, complete

                reportCompletion = (result, id, complete) ->
                    internals.routesProcessed++
                    docpad.log 'info', "Post-processing templates completed." if internals.routesProcessed is internals.routesTotal
                    complete(null, result)

                processOneRoute = (id, complete) ->
                    lookup = if _.isNumber id then internals.drupalRoutesLookup else internals.staticRoutesLookup
                    route = lookup[id]
                    url = route.docUrl

                    outpath = path.join(docpad.getConfig().outPath, url)

                    sourcePath = path.join(process.cwd(), 'src')
                    templatePath = path.join(sourcePath, 'templates')

                    document = docpad.createDocument({ meta: route })
                    docDate = new Date()
                    document.setStat({size: 0, mtime: docDate, ctime: docDate})

                    documentData = Fs.readFileSync path.join(templatePath, 'template.html.eco'), 'utf8'

                    document.setMeta(
                        layout: 'template'
                        data: documentData
                        standalone: true
                        referencesOthers: false
                        url: outpath
                        filename: 'template.html.eco'
                        menus: internals.menus
                        nid: route.nid or ''
                        pnid: route.pnid or ''
                        section: route.section or ''
                        alt: route.alt or ''
                        slug: route.slug or ''
                        language: route.language or 'en'
                        isTranslated: route.isTranslated or ''
                        translations: route.translations or ''
                        menuName: route.menuName or ''
                        docUrl: route.docUrl or ''
                        tweets: docpad.getCollection('favoritesList')?.toJSON() or []
                        prepareLinkSlug: config.templateData.prepareLinkSlug
                        objectSize: config.templateData.objectSize
                        isHttps: config.templateData.isHttps
                        getDocumentLanguage: helpers.getDocumentLanguage
                        getLanguagePrefix: helpers.getLanguagePrefix
                        i18n: helpers.i18n
                        getLocaleName: helpers.getLocaleName
                        importedPageNids: internals.importedPageNids
                        parseTweet: helpers.twitter.parseTweet
                        moment: helpers.moment
                    )

                    document.load (err) ->
                        # Check
                        if err
                            docpad.log 'error', err.stack
                            document.delete()
                            return complete(err)

                        renderOpts =
                            actions: ['renderExtensions']

                        document.render renderOpts, (err, result, document) ->
                            if err
                                docpad.log 'error', err.stack
                                document.delete()
                                return complete(err)


                            renderOutPath = (outpath + '/template.html')
                            Fs.writeFileSync renderOutPath, result

                            gulp.src renderOutPath
                            .pipe gulpTemplateHtml (outpath + '/index.html')
                            .pipe gulpRename 'index.html'
                            .pipe gulp.dest outpath.replace '/tmpl', ''

                            return reportCompletion(result, id, complete)

            # Tasks completion callback
            tasks.done (err, results) ->
                return next err, results

            # Kick of the processing
            tasks.run()

            # # Chain
            # @

        fetchRedirects: (opts={}, next) ->
            # Prepare
            plugin = @
            docpad = @docpad
            config = @getConfig()

            feed = config.feeds.redirects
            {url} = feed

            # Fetch the feed
            feedr.readFeed url, {parse:'json'}, (err, feedData) ->
                # Check
                return next(err)  if err

                internals.redirects = feedData

                _.forEach feedData, (redirect, index) ->
                    internals.redirectsRef[redirect.source] = redirect.target

                    return next()

        # Fetch our menu data
        # next(err,menuData)
        fetchMenuItems: (opts={}, next) ->
            # Prepare
            plugin = @
            docpad = @docpad
            config = @getConfig()

            feeds = config.feeds.menu

            collections = []
            linksLookup = {}

            feedKeys = Object.keys(feeds)
            feedsTotal = feedKeys.length
            feedsProcessed = 0

            feedKeys.forEach (feed, menuKey) ->
                # Prepare
                links = []
                me = {}

                {url, collectionName} = feeds[feed]

                me.getLinksRecursive = (link) ->
                    # Ensure no duplicates
                    if link?.link?.nid?
                        unless linksLookup[link.link.nid]
                            linksLookup[link.link.nid] = link.link
                            links.push(link.link)

                    if link.children?
                        _.forEach link.children, (link, key) ->
                            me.getLinksRecursive(link)

                me.checkForMenuChanges = (newMenu, oldMenu) ->
                    if newMenu? and oldMenu?
                        if newMenu.length isnt oldMenu.length
                            docpad.log 'info', "#{collectionName}: Menu Length Changed"
                            internals.menuChanged = true
                            return

                        else
                            menus = [newMenu, oldMenu]
                            menus.forEach (menu, menuIndex) ->
                                altMenuIndex = if menuIndex is 0 then 1 else 0
                                checking = if menuIndex is 0 then 'New' else 'Old'

                                menu.some (link, index) ->
                                    changeDetected = false
                                    altLink = menus[altMenuIndex][index]

                                    compareProperties = ['mlid', 'depth', 'title', 'slug']
                                    compareProperties.some (property) ->
                                        if link[property] isnt altLink[property]
                                            docpad.log 'info', "#{collectionName}: Menu Property Changed (#{property}):  #{menus[0][index][property]} was #{menus[1][index][property]}"
                                            internals.menuChanged = true
                                            return true # Break out of the some loop by returning true

                # Fetch the feed
                feedr.readFeed url, {parse:'json'}, (err, feedData) ->
                    # Check
                    return next(err)  if err

                    # Save reference to entire menu.  Will be added to templateData later
                    internals.menus[feed] = feedData

                    feedsProcessed++

                    # Check the feed's data
                    unless feedData?.tree?
                        docpad.log 'warn', "Menu feed data for '#{collectionName}' was empty"

                    # Create array of links for which pages need to be generated
                    _.forEach feedData.tree, (link, key) ->
                        me.getLinksRecursive link

                    collection =
                        name: collectionName
                        links: links

                    # Check for changes to the menus (skip this check if we have already detected a change)
                    me.checkForMenuChanges collection.links, internals.menuRefPrev[feed] unless internals.menuChanged or (collectionName is 'generic')

                    # Save internal reference of this menu for later comparison
                    internals.menuRef[feed] =  collection.links

                    collections.push(collection)

                    if feedsProcessed is feedsTotal
                        return next(null, collections)


        # next(err, document)
        createDocument: (page, link, next) ->
            # Prepare
            plugin = @
            docpad = @docpad
            config = @getConfig()

            # Extract
            {nid, type, title, created, changed, revision_timestamp, metatags, url, pageLayout, language, section} = page
            {pnid, slug, isTranslated, translations, menuName} = link

            if !pageLayout then pageLayout = 'default'

            # Treat undefined language as default english
            if language is 'und'  or  not language?
                language = 'en'

            # Prepare dates
            createdDate = new Date(created * 1000)
            changedDate = new Date((revision_timestamp or changed) * 1000)

            filename = "#{slug}#{config.extension}"
            name = filename.replace('.eco', '')
            relativePath = "#{config.relativeDirPath}/#{type}/#{language}/#{filename}"

            # Add Page Metadata
            meta =
                nid: nid
                pnid: pnid
                type: type
                title: title
                ctime: createdDate
                mtime: changedDate
                relativePath: relativePath
                filename: filename
                name: name
                outFilename: name
                slug: slug
                url: "/#{language}/#{url}/tmpl"
                page: page
                drupalPage: true
                language: language
                isTranslated: isTranslated
                translations: translations
                menuName: menuName
                pageLayout: pageLayout
                standalone: true
                referencesOthers: true

            # Don't need the path prefix on english √pages
            if language is 'en'
                meta.url = "/#{url}/tmpl"

                # Need to set flag to remove existing index page if we pull one in from drupal
                if slug is 'index'
                    meta.relativePath = "#{slug}#{config.extension}"

            for key, tags of metatags
                if tags.title?.value
                    meta.pageTitle = tags.title.value

                if tags.description?.value
                    meta.description = tags.description.value

                if tags.keywords?.value
                    meta.keywords = tags.keywords.value

            # Add nid to importedPageNids array
            internals.importedPageNids.push(nid)

            if section? and section in config.forceGulpTemplateSections
                # Add nid to forceGulpTemplateNids array
                internals.forceGulpTemplateNids.push(nid)

            # Populate tourOptions
            if section? and section is 'tour-options'
                options = []

                if page.field_page_content_panels?.und?
                    panels = page.field_page_content_panels.und

                    panels.forEach (panel, index) ->
                        node = panel.node
                        if node? and node.field_custom_content_panel_type?.und?[0]?.slug is 'tour-option'
                            data =
                                title: node.title
                                nid: node.nid
                                externalLink: node.field_content_panel_link?.und?[0]?.url or false

                            options.push data

                internals.tourOptions[language] =
                    title: meta.title
                    url: meta.url
                    nid: meta.nid
                    options: options

            # Fetch existing document
            document = docpad.getFile({nid: meta.nid})

            # Check if it's outDated
            documentTime = document?.toJSON().meta.mtime or document?.toJSON().meta.ctime null
            outDated = documentTime and documentTime.toString() isnt changedDate.toString()

            # Compare
            # if document? and !outDated and !internals.menuChanged
            if document? and !outDated
                # Skip if it's not outdated and the menu hasn't changed
                docpad.log 'debug', 'Skipped: ' + nid
                return next(null, null)

            # Prepare
            documentAttributes =
                meta: meta
                outDated: outDated

            # Existing document
            if document?
                document.set(documentAttributes)

            # New Document
            else
                # Create document from opts
                document = docpad.createDocument(documentAttributes)

            # Need to set document.stat.size to avoid error in hapi-server.
            document.setStat({size: 0, mtime: changedDate, ctime: createdDate})

            # Inject document helper
            config.injectDocumentHelper?.call(plugin, document)

            document.action 'load', (err) ->
                # Check
                return next(err) if err

                # Add it to the database (with b/c compat)
                docpad.addModel?(document) or docpad.getDatabase().add(document)

                # Save a reference of this document having been updated
                internals.updatedPageNids.push nid

                return next(null, document)


        # Populate Collections
        # Import Drupal Pages into the Database
        populateCollections: (opts, next) ->
            # Prepare
            plugin = @
            docpad = @docpad
            config = @getConfig()
            internals.menuChanged = false

            pageFeedUrl = config.feeds.page.url
            pageFeedUrlSuffix = if internals.preview then '?preview=1' else ''

            # Log
            docpad.log 'info', "Importing Pages from Drupal Menu Collections..."

            @fetchRedirects null, (err, collections) ->
                return next(err)  if err

            # Fetch
            @fetchMenuItems null, (err, collections) ->
                # Check
                return next(err)  if err

                # Prepare
                me = {}
                imported = {}

                me.reportProgress = (err, collectionName) ->
                    # Check
                    return next(err)  if err

                    collectionsProcessed++

                    # Log
                    docpad.log 'info', "Imported #{imported[collectionName]} Pages from #{collectionName} collection..."

                    # Complete
                    if collectionsProcessed is collectionsTotal
                        # Log
                        docpad.log 'info', "Imported #{totalPagesPulled} Total Pages..."

                        if internals.menuChanged or config.forceGulpTemplateSections.length
                            docpad.log 'info', "Menu Change Detected... All page menus will be rebuilt." if internals.menuChanged

                            # Let's update the menu layout, so we can trigger a rebuild of all the pages' menus
                            database = docpad.getDatabase()
                            fileQuery =
                                drupalTemplateLayout: true

                            templates = docpad.getFiles(fileQuery)

                            templates.forEach (template) ->
                                template.setStat({mtime: new Date()})
                                database.add(template)

                        return next()

                collectionsTotal = collections.length
                collectionsProcessed = 0
                totalPagesPulled = 0

                # Inject our pages
                _.forEach collections, (collection, i) ->
                    # Prepare
                    {links, name} = collection

                    # Imported
                    imported[name] = 0
                    linksProcessed = 0
                    linksTotal = links.length

                    # Inject our pages
                    Async.each(
                        links,
                        ((link, callback) ->

                            if link.nid?
                            # if link.item?  and  link.item.type is 'page'
                                nid = link.nid
                                # Fetch the feed
                                feedr.readFeed "#{pageFeedUrl}/#{nid}.json#{pageFeedUrlSuffix}", {parse:'json'}, (err, page) ->
                                    if page?
                                        # Create
                                        plugin.createDocument page, link, (err, document) ->
                                            # Check
                                            return callback(err)  if err

                                            # Log
                                            if document
                                                imported[name]++
                                                totalPagesPulled++
                                                docpad.log 'debug', "Imported page #{page.nid}: #{document.getFilePath()}"
                                            else
                                                docpad.log 'debug', "Skipped page #{page.nid} of #{name} collection"

                                            linksProcessed++

                                            if linksProcessed is linksTotal
                                                return me.reportProgress(null, name)
                                    else
                                        callback()


                            else
                                # Complete
                                callback()
                        ),
                        (err, result) ->
                            # Check
                            return next(err)  if err
                    )
